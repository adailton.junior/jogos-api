'use strict';
const {
  Model, Op
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Jogos extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }

  Jogos.init({
    nome: DataTypes.STRING,
    categoria: DataTypes.STRING,
    ano: DataTypes.STRING,
    descricao: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Jogos',
  });

  Jogos.findByCategoria = async (categoria, offset, limit = 5) => {
    return await Jogos.findAndCountAll({
        where: {
            [Op.and]: [
                categoria && {categoria: { [Op.eq]: categoria.toUpperCase() }}
            ]
        },
        offset: (offset*limit),
        limit: limit
    });
}

  return Jogos;
};
