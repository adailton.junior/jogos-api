const customExpress = require('./config/customExpress');
const { Jogos } = require('./models');

const app = customExpress();

process.env.NODE_ENV = 'development';

app.listen(3000, async () => {
    console.log('API de jogo em execução na porta 3000');

    // const jogos = await Jogos.findAll();
    // console.log("All jogos:", JSON.stringify(jogos, null, 2));
});

app.get('/', (request, response) => response.send("API  de Jogos Version 1.0 on-line"));

