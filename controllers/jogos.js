const { Jogos } = require('../models');

module.exports = app => {
    app.get('/jogos', async (request, response) => {
        // A paginação inicia do 0 (zero)
        if (request.query.pagina === null || request.query.pagina === undefined) {
            request.query.pagina = 0;
        }
        const { count, rows } = await Jogos.findByCategoria(request.query.categoria, request.query.pagina);
	
	response.header("Access-Control-Allow-Origin", "*");
        response.send({ 
            status: 'success', 
            message: 'Lista de jogos encontrado',
            total: count,
            jogos: rows
        })
    });

    app.get('/jogos/:id', async (request, response) => {        
        const jogo = await Jogos.findByPk(request.params.id);
        
        response.header("Access-Control-Allow-Origin", "*");
        if (jogo !== null && jogo !== undefined) {
            response.send({ 
                status: 'success', 
                message: 'Jogo encontrado', 
                jogo
            });
        } else {
            response.send({ 
                status: 'error', 
                message: 'Jogo não encontrado', 
            });
        }
    });

    app.post('/jogos', (request, response) => {
        response.header("Access-Control-Allow-Origin", "*");
        if (request.body.nome === undefined
        || request.body.ano === undefined
        || request.body.categoria === undefined) {
            response.status(403);
            response.send({ status: 'error', message: `Campos nome, ano e categoria são obrigatórios`});
            return;
        }

        Jogos.create({
            nome: request.body.nome,
            ano: request.body.ano,
            categoria: request.body.categoria.toUpperCase(),
            descricao: request.body.descricao,
        }).then((jogo) => {
            response.send({
                status: 'success',
                message: 'Jogo criado!',
                jogo
            })
        }).catch((error) => {
            console.log(error.name);
            if (error.name === 'SequelizeUniqueConstraintError') {
                response.status(403)
                response.send({ status: 'error', message: `Jogo ${request.body.nome} já cadastrado`});
            } else {
                response.status(500);
                response.send({
                    status: 'error', message: 'Error não identificado'
                });
            }
        });
    });
}
