'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return queryInterface.bulkInsert('Jogos', [{
      nome: 'Super Mario World',
      ano: '1990',
      categoria: 'PLATAFORMA',
      descricao: 'Super Mario World, originalmente chamado no Japão de Super Mario Bros. 4, é um jogo eletrônico de plataforma desenvolvido pela Nintendo Entertainment Analysis & Development e publicado pela Nintendo, em 1990, para o console Super Nintendo Entertainment System.',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      nome: 'Donkey Kong Country',
      ano: '1994',
      categoria: 'PLATAFORMA',
      descricao: 'Donkey Kong Country é um jogo de plataforma de 1994 desenvolvido pela Rare e publicado pela Nintendo para o Super Nintendo Entertainment System.',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      nome: 'Sonic the Hedgehog',
      ano: '1991',
      categoria: 'PLATAFORMA',
      descricao: 'Sonic the Hedgehog é uma série de jogos e uma franquia multimídia japonesa criada pelo Sonic Team e produzida pela Sega.',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      nome: 'Contra',
      ano: '1987',
      categoria: 'PLATAFORMA',
      descricao: 'Contra, é um videogame do gênero plataforma lançado para arcade no ano de 1987 pela fabricante japonesa de jogos eletrônicos Konami.',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      nome: 'Half-life',
      ano: '1998',
      categoria: 'FPS',
      descricao: 'Half-Life é uma série de jogos de tiro em primeira pessoa, que dividem uma história alternativa de ficção científica.',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      nome: 'Half-life 2',
      ano: '2004',
      categoria: 'FPS',
      descricao: 'Half-Life é uma série de jogos de tiro em primeira pessoa, que dividem uma história alternativa de ficção científica.',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      nome: 'The Elder Scrolls V: Skyrim',
      ano: '2011',
      categoria: 'MUNDO ABERTO',
      descricao: 'The Elder Scrolls V: Skyrim é um jogo eletrônico de RPG de ação desenvolvido pela Bethesda Game Studios e publicado pela Bethesda Softworks.',
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete('Jogos', null, {});
  }
};

